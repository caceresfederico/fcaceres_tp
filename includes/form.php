    <!-- Se incluyen los valores dentro de $_SESSION -->
    <?php include 'save.php'; ?>
    <!-- Sección para formulario -->
    <section>
        <!-- Validación php para la sesión -->
        <?php if (isset($_SESSION['mensaje'])) { ?>
            <div class="alert alert-<?= $_SESSION['colorMensaje'] ?> alert-dismissible fade show border border-secondary" id="mensajeUsuario" role="alert"><?= $_SESSION['mensaje'] ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- Se eliminan los valores de $_SESSION para que no se muestren cada vez que se carga la página -->
        <?php session_unset();
        } ?>
        <div class="container-fluid text-white ">
            <div class="row align-items-center">
                <!-- Primera columna formulario -->
                <div class="col-12 col-md-12 col-lg-6 col-sm-12" id="firstCol">
                    <div class="container justify-content-center pt-3">
                        <form action="./includes/save.php" method="POST">
                            <h2>Registrar usuario</h2>
                            <div class="form-group ">
                                <label for="name">Nombre:</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Nombre" autofocus>
                            </div>
                            <div class="form-group ">
                                <label for="surname">Apellido:</label>
                                <input type="text" class="form-control" name="surname" id="surname" placeholder="Apellido">
                            </div>
                            <div class="form-group">
                                <label for="documentType">Tipo de documento</label>
                                <select name="documentType" id="documentType">
                                    <option value="dni" selected name="dni">DNI</option>
                                    <option value="carnetExtranjero" name="carnetExtranjero">Carnet extranjero</option>
                                    <option value="partidaNacimiento" name="partidaNacimiento">Partida de nacimiento</option>
                                    <option value="pasaporte" name="pasaporte">Pasaporte</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="documentNumber">Numero de documento:</label>
                                <input type="number" class="form-control" name="documentNumber" id="documentNumber">
                            </div>
                            <div class="form-group">
                                <label for="adress">Dirección:</label>
                                <input type="text" class="form-control" name="adress" id="adress">
                            </div>
                            <div class="form-group">
                                <label for="adressNumber">Altura:</label>
                                <input type="number" class="form-control" name="adressNumber" id="adressNumber">
                            </div>
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" class="form-control" name="email" id="email">
                            </div>
                            <div class="form-group">
                                <label for="phone">Teléfono:</label>
                                <input type="number" class="form-control" name="phone" id="phone">
                            </div>
                            <div class="form-group">
                                <label for="birthdate">Fecha de nacimiento:</label>
                                <input type="date" class="form-control" name="birthdate" id="birthdate">
                            </div>
                            <div class="form-group">
                                <label for="rolSelect">Rol:</label>
                                <select name="rolSelect" id="rolSelect">
                                    <option value="alumno" name="alumno" selected>Alumno</option>
                                    <option value="docente" name="docente">Docente</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary" name="save">Submit</button>
                        </form>
                    </div>
                </div>
                <!-- Segunda columna imagen para formulario -->
                <div class="col-12 col-md-12 col-lg-6 col-sm-12 p-0">
                    <div class="container-fluid p-0" id="formImg">
                    </div>
                </div>
            </div>
        </div>
    </section>