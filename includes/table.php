<?php include 'db.php' ?>
<!-- TABLA -->
<section>
  <div class="container-fluid p-0" id="tableContainer">
    <table class="table">
      <thead class="thead-dark">
        <tr>
          <th scope="col">ID</th>
          <th scope="col">Nombre</th>
          <th scope="col">Apellido</th>
          <th scope="col">Tipo Doc.</th>
          <th scope="col">Num. Doc.</th>
          <th scope="col">Dirección</th>
          <th scope="col">Altura</th>
          <th scope="col">Email</th>
          <th scope="col">Teléfono</th>
          <th scope="col">Fecha nacimiento</th>
          <th scope="col">Rol</th>
          <th scope="col">Acción</th>
        </tr>
      </thead>
      <tbody>
          <?php 
            $query = "SELECT * FROM users";
            $result_query = mysqli_query($conn, $query);

            while($row = mysqli_fetch_array($result_query)){ ?>
            <tr>
              <td><?php echo $row['idUser']?></td>
              <td><?php echo $row['nameUser']?></td>
              <td><?php echo $row['surnameUser']?></td>
              <td><?php echo $row['documentType']?></td>
              <td><?php echo $row['dniNumber']?></td>
              <td><?php echo $row['adressUser']?></td>
              <td><?php echo $row['adressNumberUser']?></td>
              <td><?php echo $row['emailUser']?></td>
              <td><?php echo $row['phoneUser']?></td>
              <td><?php echo $row['birthdateUser']?></td>
              <td><?php echo $row['rolUser']?></td>
              <td>
                <a href="./edit.php?id=<?php echo $row['idUser']?>" class="btn btn-secondary"><i class="fas fa-marker"></i></a>
                <a href="./delete.php?id=<?php echo $row['idUser']?>" class="btn btn-danger"><i class="far fa-trash-alt"></i></a>
              </td>
            </tr>

           <?php }?>
          
      </tbody>
    </table>
  </div>
</section>