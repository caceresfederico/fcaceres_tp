<?php
include "db.php";
// Se chequea si se estan enviando los datos y dentro del cuerpo del if se guardarn los datos en variables.
if(isset($_POST['save'])){
    $name = $_POST['name'];
    $surname = $_POST['surname'];
    $document = $_POST['documentType'];
    $documentNumber = $_POST['documentNumber'];
    $adress = $_POST['adress'];
    $adressNumber = $_POST['adressNumber'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $birthdate = $_POST['birthdate'];
    $rol = $_POST['rolSelect'];
    // Creación de la query para guardar los datos en la db
    $query = "INSERT INTO users (nameUser, surnameUser, documentType, dniNumber, adressUser, adressNumberUser, emailUser, phoneUser, birthdateUser, rolUser) VALUES ('$name','$surname','$document','$documentNumber','$adress','$adressNumber','$email','$phone','$birthdate','$rol')";
    // Realización de la query con la db
    $resultado = mysqli_query($conn, $query);
    // Prueba de resultado de la conexión
    if(!$resultado){
        die('Fallo la query');
    }
    // Guardado de valores dentro de $_SESSION
    $_SESSION['mensaje'] = 'Usuario agregado exitosamente!';
    $_SESSION['colorMensaje'] = 'success';
    // Relocación para que se mantenga dentro del index y no cambie a otra página del sitio
    header("Location: ../index.php");
}
